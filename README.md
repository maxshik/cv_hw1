# CV_HW1

HW1 Thousand Facial Landmarks

![alt text](./image.png "Leaderboard score")

Difference from baseline:

1. Additional transformers for training:

    * ```RotationTransform()``` the script in ```hack_utils.py```

    ```python
    class RotationTransform:
        """Rotate by one of the given angles."""

        def _find_center(self, image):
            img = np.array(image)
            return (img.shape[1]//2, img.shape[0]//2)

        def _rotation_matrix(self, alpha):
            a = np.deg2rad(alpha)
            Q = torch.tensor([
                    [np.cos(a), -np.sin(a)],
                    [np.sin(a), np.cos(a)],
                ])
            return Q.float()

        def _rotate_landmark(self, image, landmarks, alpha):
            center = torch.as_tensor(self._find_center(image))[None, ...]
            v = landmarks.clone()
            v -= center
            Q = self._rotation_matrix(alpha)
            return torch.mm(v, Q) + center.float()

        def __call__(self, sample):
            angle = np.random.randint(-10, 10)
            output = sample.copy()
            output['image'] = transforms.functional.rotate(sample['image'], angle)
            landmarks = sample['landmarks'].reshape(-1, 2)
            output['landmarks'] = self._rotate_landmark(sample['image'], landmarks, angle).reshape(-1)
            return output
    ```

    * ```ColorJitter()```

2. Changed ```resnet18``` to ```resnet50```

To run: ```python hack_train.py --name "augment_resnet50" --data "PATH_TO_DATA" --gpu --epochs 30 --batch-size 200```
